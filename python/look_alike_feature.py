from typing import Mapping

import scipy.sparse as sp
import numpy as np
import h5sparse

from aixon.sources.data_handlers.utils.dmp_data import DmpData


def csr_transform_column_index(
        matrix: sp.csr_matrix, column_index_map: Mapping[int, int], n_columns: int):
    """Transform a CSR matrix to a new column order.

    Parameters
    ----------
    matrix: sp.csr_matrix
        The original matrix.
    column_index_map: Mapping[int, int]
        Map from old index to new index. If a new column is not mapped by any old column, it will
        be all zero.
    n_columns: int
        The number of columns in the new matrix. It should be greater or equal to the max value in
        ``column_index_map``.
    """
    # map column indices
    old_col_idx, new_col_idx = list(zip(*sorted(column_index_map.items())))
    use_data = np.isin(matrix.indices, old_col_idx)
    csr_indices = np.take(
        new_col_idx,
        np.searchsorted(old_col_idx, matrix.indices[use_data]))

    # filter data
    csr_data = matrix.data[use_data]

    # build indptr
    csr_indptr = np.empty_like(matrix.indptr)
    csr_indptr[0] = 0
    indptr = 0
    row_indptr_start = matrix.indptr[0]
    for row_i, row_indptr_end in enumerate(matrix.indptr[1:]):
        indptr += np.count_nonzero(use_data[row_indptr_start:row_indptr_end])
        csr_indptr[row_i + 1] = indptr
        row_indptr_start = row_indptr_end
    assert csr_indptr[-1] == csr_indices.shape[0]

    # build CSR matrix
    result_matrix = sp.csr_matrix(
        (csr_data, csr_indices, csr_indptr),
        shape=(matrix.shape[0], n_columns), dtype=np.float32)
    result_matrix.sort_indices()
    return result_matrix


def iter_testing_feature_in_h5(batch_size=1000000):
    dmp_data = DmpData('/nfs/lookalike/tw/2018-12-21/')
    dmp_h5_path = '/nfs/lookalike/tw/2018-12-21/data.h5'

    with h5sparse.File(dmp_h5_path, 'r') as h5f:
        # build the map from full index to used index
        column_names = np.char.decode(h5f['column_names'].value, 'utf-8')
        col_idx_map = {col: col_i for col_i, col in enumerate(column_names)}
        # map from the column idx in h5 to the column idx used in training
        used_idx_map = {col_idx_map[col]: col_i
                        for col_i, col in enumerate(column_names)
                        if col in col_idx_map}

        dmp_data_shape = h5f['data'].h5py_group.attrs['h5sparse_shape']
        for batch_i, batch_start in enumerate(range(0, dmp_data_shape[0], batch_size)):
            batch_end = min(dmp_data_shape[0], batch_start + batch_size)
            print("offset: {}~{}".format(batch_start, batch_end))

            # get one batch of data
            batch_data = h5f['data'][batch_start: batch_end]

            # select the used features and build a new CSR sparse matrix
            batch_features = csr_transform_column_index(batch_data, used_idx_map, len(column_names))

            batch_device_idx = np.arange(batch_start, batch_end, dtype=np.int64)
            batch_device_ids = dmp_data.convert_device_dmp_h5_idx_to_ids(batch_device_idx)

            yield batch_features, batch_device_ids

def remove_id_prefix(input_id):
    return input_id.split("::")[1]

def read_id_list(id_list_paths):
    id_list = set()
    for id_list_path in id_list_paths:
        with open(id_list_path, 'r', encoding='utf-8') as id_list_f:
            for line in id_list_f:
                id_list.add(line.strip().split(" ")[0])
            print("read {} done.".format(id_list_path))

    return id_list

if __name__ == "__main__":
    #需要的只有campaign_train和campaign_test
    #就算未來突然需要campaign_current retarget_test retarget_current了,也都已經被包含了
    #坦白說好像有些沒包含,但是理論上不該有,算了先不管他,反正應該也是少量,目前有些機制是找不到feature會跳過這筆
    all_id_list = read_id_list(
        [
            '/nfs/tmp/sega_campaign_train.txt', 
            '/nfs/tmp/sega_campaign_test.txt', 
            '/nfs/tmp/neogence_campaign_train.txt', 
            '/nfs/tmp/neogence_campaign_test.txt', 
            '/nfs/tmp/cathaybk_campaign_train.txt', 
            '/nfs/tmp/cathaybk_campaign_test.txt', 
            '/nfs/tmp/igs_tw_campaign_train.txt', 
            '/nfs/tmp/igs_tw_campaign_test.txt', 
            '/nfs/tmp/igs_jp_campaign_train.txt', 
            '/nfs/tmp/igs_jp_campaign_test.txt'
        ]
    )

    lookalike_feature_f = open('/nfs/tmp/look_alike_features', 'w', encoding='utf-8')
    log_count = 0
    for features, device_ids in iter_testing_feature_in_h5():
        for i in range(0, len(device_ids)):
            if remove_id_prefix(device_ids[i]) in all_id_list:
                non_zero_features = sp.find(features[i])
                feature_idxs = non_zero_features[1].tolist()
                feature_values = non_zero_features[2].tolist()

                f_str = " ".join(
                    list(map(
                        lambda x: "{}:{}".format(x[0], x[1]), 
                        list(zip(feature_idxs, feature_values))
                    ))
                )

                lookalike_feature_f.write("{} {}\n".format(device_ids[i], f_str))  
                log_count += 1

        print(log_count)
    lookalike_feature_f.close()
