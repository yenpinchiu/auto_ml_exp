import shelve
import random

from utils import *

def create_campaign_db(campaign_data_path, campaign_db_name, sample_rate = 1.0, adid_lists = None):
    campaign_db = shelve.open(campaign_db_name)
    campaign_data_f = open(campaign_data_path, 'r', encoding='utf-8')

    for line in campaign_data_f:
        line_split = line.strip().split("||")
        adid = line_split[0]

        if sample_rate < 1.0 and random.random() > sample_rate:
            continue
        
        if adid_lists is not None:
            in_flag = False
            for adid_list in adid_lists:
                if adid in adid_list:
                    in_flag = True
            
            if not in_flag:
                continue

        tmp = [0, 0, 0, 0, []]
        for metric_str in line_split[1:]:
            metric = metric_str.split("|")
            tmp[0] += int(metric[1])
            tmp[1] += int(metric[2])
            tmp[2] += int(metric[3])
            tmp[3] += int(metric[4])

            #要有click才表示對該campaign type有興趣,才會被加入adid campaign type list內
            #有click就可不一定要click after show
            if int(metric[3]) != 0:
                tmp[4].append(metric[0])
        
        campaign_db[adid] = tmp

    campaign_data_f.close()

    print("campaign db {} created.".format(campaign_db_name))
    return campaign_db

def create_retarget_db(retarget_data_paths, retarget_db_name, sample_rate = 1.0, adid_lists = None):
    retarget_db = shelve.open(retarget_db_name)
    
    for retarget_data_path in retarget_data_paths:
        retarget_data_f = open(retarget_data_path, 'r', encoding='utf-8')
        for line in retarget_data_f:
            adid = line.strip()
            
            if sample_rate < 1.0 and random.random() > sample_rate:
                continue
            
            if adid_lists is not None:
                in_flag = False
                for adid_list in adid_lists:
                    if adid in adid_list:
                        in_flag = True
                
                if not in_flag:
                    continue

            retarget_db[adid] = True

        retarget_data_f.close()

    print("retarget db {} created.".format(retarget_db_name))
    return retarget_db

def create_feature_db(feature_data_path, feature_db_name, sample_rate = 1.0, adid_lists = None):
    feature_db = shelve.open(feature_db_name)
    feature_data_f = open(feature_data_path, 'r', encoding='utf-8')

    for line in feature_data_f:
        line_split = line.strip().split(" ")
        adid = line_split[0].split("::")[1]

        if sample_rate < 1.0 and random.random() > sample_rate:
            continue
        
        if adid_lists is not None:
            in_flag = False
            for adid_list in adid_lists:
                if adid in adid_list:
                    in_flag = True
            
            if not in_flag:
                continue

        features = {}
        for feature in line_split[1:]:
            feature_split = feature.split(":")
            f_idx = feature_split[0]
            f_val = feature_split[1]

            if f_val != 'nan':
                features.update({int(f_idx): int(float(f_val))})

        feature_db[adid] = features

    feature_data_f.close()

    print("feature db {} created.".format(feature_db_name))
    return feature_db

def db_test(db):
    c = 0
    for k in db:
        if c < 10:
            print(k, db[k])
        c += 1
    print(c)

if __name__ == "__main__":
    company = "cathaybk"

    #train campaign db
    train_campaign_db = create_campaign_db("{}_campaign_train.txt".format(company), "{}_train_campaign_db".format(company))
    db_test(train_campaign_db)

    #test campaign db
    #test很多,以作實驗來講不用那麼多,故sample之,因為量非常大所以基本上不會有sample偏斜
    test_campaign_db = create_campaign_db("{}_campaign_test.txt".format(company), "{}_test_campaign_db".format(company), sample_rate = 0.25)
    db_test(test_campaign_db)

    #current campaign db
    current_campaign_db = create_campaign_db("{}_campaign_current.txt".format(company), "{}_current_campaign_db".format(company))
    db_test(current_campaign_db)

    #retarget adid list
    retarget_db = create_retarget_db(
        [
            "{}_retarget_test.txt".format(company), 
            "{}_retarget_current.txt".format(company)
        ], 
        "{}_retarget_db".format(company)
    )
    db_test(retarget_db)

    #features
    #只產生在test和train內的就好
    feature_db = create_feature_db(
        "look_alike_features.txt", 
        "{}_feature_db".format(company), 
        adid_lists = [train_campaign_db, test_campaign_db, current_campaign_db]
    )
    db_test(feature_db)

    train_campaign_db.close()
    test_campaign_db.close()
    current_campaign_db.close()
    retarget_db.close()
    feature_db.close()
