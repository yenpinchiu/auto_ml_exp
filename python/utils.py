def build_train_features(train_adids, adid_features):
    train_features = {}
    train_adid_count = 0
    for adid in train_adids:
        if adid in adid_features:
            adid_feature = process_feature(adid_features[adid])
            for f_idx in adid_feature:
                if f_idx not in train_features:
                    train_features.update({f_idx: 0})
                train_features[f_idx] += adid_feature[f_idx] #使用frequency
                #train_features[f_idx] += 1 #不使用frequency
            
            train_adid_count += 1
    
    for feature in train_features:
        train_features[feature] /= train_adid_count
    
    return train_features

#某些feature比較特別,做一些處理
def process_feature(feature):
    #gender
    #idx = 0的gender基本上只有值就是1,估計是可能有值就是某個性別,沒有就是另一個
    if 0 not in feature:
        feature.update({"no_0": 1})

    #idx = 1的age感覺是切bin的值,故打散成cate
    if 1 in feature:
        feature.update({"1_{}".format(feature[1]): 1})
        del feature[1]

    return feature

#這邊先用cosine similarity
#其實不確定到底合不合理,比方說如果有個維度整個train set只有一個adid有,雖然value會很低,但結論上test adid有這個維度的score會比沒這個score高一點,感覺怪怪的
#要解決以上問題可改用Euclidean distance取倒數當score等等
def caculate_similarity_score(adid, feature, feature2):
    similarity_score = 0
    for f_idx in feature:
        if f_idx in feature2:
            similarity_score += feature[f_idx] * feature2[f_idx] #使用frequency
            #similarity_score += feature2[f_idx] #不使用frequency

    similarity_score /= len(feature) #這邊cosine similarity不再除len(feature2),畢竟每個adid的score都除len(train_features)等於沒除

    return similarity_score

def caculate_ctr_cvr_score(adid, metric):
    show_num = metric[0]
    click_after_show_num = metric[1]
    ctr = click_after_show_num / show_num if show_num != 0 else 0.0
    #這邊的想法是,一群adid的total show和total click after show形成這群adid的ctr
    #而一個新的adid只要該adid之ctr大於那群adid的ctr,則此adid被加入該群時一定會對該群ctr有所提升
    #因此直接用單個adid的ctr當score,越大越好
    ctr_cvr_score = ctr #目前只用ctr

    return ctr_cvr_score, click_after_show_num, show_num

def caculate_retarget_score(adid, retarget_id_list):
    is_retarget = True if adid in retarget_id_list else False
    retarget_score = 1.0 if is_retarget else 0.0

    return retarget_score, is_retarget

def build_camp_type_list(campaign_dbs):
    camp_list = set()
    for campaign_db in campaign_dbs:
        for adid in campaign_db:
            for camp_type in campaign_db[adid][4]:
                camp_list.add(camp_type)

    return camp_list

class Statistics:
    #metric
    output_click_after_show_num_total = 0
    output_show_num_total = 0
    output_retarget_total = 0

    #統計數據
    output_adid_count = 0
    score_total = 0
    similarity_score_total = 0
    ctr_cvr_score_total = 0
    retarget_score_total = 0

    output_score_total = 0
    output_similarity_score_total = 0
    output_ctr_cvr_score_total = 0
    output_retarget_score_total = 0

def current_metric_point(train_campaign_db, test_campaign_db, current_campaign_db, retarget_db):
    #計算current list的acc, ctr和retarget coverage
    #acc
    in_train_test_num = 0
    in_train_test_current_num = 0
    for adid in train_campaign_db:
        #有在test中才可能被加入current,因此只算這些有多少被加入current
        #然而因為test會sample,沒進去的有可能單純只是沒sample到,不過即使如此,仍不影響只算那些有sample到的的正確性
        if adid in test_campaign_db:
            if adid in current_campaign_db:
                in_train_test_current_num += 1
            in_train_test_num += 1

    adid_total = 0
    total_show_num = 0
    total_click_after_show_num = 0
    retarget_num = 0
    camp_cate_his = {}
    for adid in current_campaign_db:
        show_num = current_campaign_db[adid][0]
        click_after_show_num = current_campaign_db[adid][1]
        
        total_show_num += show_num
        total_click_after_show_num += click_after_show_num

        if adid in retarget_db:
            retarget_num += 1

        adid_camp_types = current_campaign_db[adid][4]
        adid_camp_types_num = len(adid_camp_types)
        for camp_cate in adid_camp_types:
            if camp_cate not in camp_cate_his:
                camp_cate_his.update({camp_cate: 0.0})
            camp_cate_his[camp_cate] += 1 / adid_camp_types_num

        adid_total += 1

    print("{}|{}|{}|{}|current".format(
            in_train_test_current_num/in_train_test_num, 
            total_click_after_show_num/total_show_num, 
            retarget_num/adid_total, 
            camp_cate_his
        )
    )
