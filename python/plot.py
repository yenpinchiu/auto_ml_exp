from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
import math

import ast

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

point_list = []
total_acc = 0
total_ctr = 0
total_retarget = 0
total_point_num = 0
para_metric_file = open("../data/cathaybk_point_metric.txt", 'r', encoding='utf-8')
for line in para_metric_file:
    line_split = line.strip().split("|")
    acc = 1/float(line_split[0]) #取倒數,越接近0越好
    ctr = 1/float(line_split[1])
    retarget = 1/float(line_split[2])

    if len(line_split) >= 5 and line_split[4] == "current":
        is_current = True
    else:
        is_current = False

    #distance還沒算
    not_yet_caculated_distance = 0
    is_min_distance = False

    camp_cate_his_str = line_split[3]

    point_list.append([acc, ctr, retarget, is_current, is_min_distance, not_yet_caculated_distance, camp_cate_his_str])

    total_acc += acc
    total_ctr += ctr
    total_retarget += retarget
    total_point_num += 1
para_metric_file.close()

acc_n = 1 / (total_acc / total_point_num)
ctr_n = 1 / (total_ctr / total_point_num)
retarget_n = 1 / (total_retarget / total_point_num)
for p in point_list:
    p[0] = p[0] * acc_n
    p[1] = p[1] * ctr_n
    p[2] = p[2] * retarget_n

    distance = math.sqrt(math.pow(p[0] , 2) + math.pow(p[1], 2) + math.pow(p[2], 2))
    p[5] = distance

min_distance = min([p[5] for p in point_list])

for p in point_list:
    if p[5] == min_distance:
        p[4] = True

for p in point_list:
    if p[3]: #current會把min蓋過去
        ax.scatter(p[0], p[1], p[2], c='b' , marker='o', s=30)
        ax.text(p[0], p[1], p[2], "{}, {}, {}".format(round(p[0], 3), round(p[1], 3), round(p[2], 3)))

        sorted_camp_type_dict = sorted(ast.literal_eval(p[6]).items(), key=lambda kv: kv[1], reverse=True)
        t = 0
        for k in sorted_camp_type_dict:
            t += k[1]
        
        for k in sorted_camp_type_dict:
            print(k[0], k[1]/t)

        print("----------")
    elif p[4]:
        ax.scatter(p[0], p[1], p[2], c='g' , marker='o', s=30)
        ax.text(p[0], p[1], p[2], "{}, {}, {}".format(round(p[0], 3), round(p[1], 3), round(p[2], 3)))
        
        sorted_camp_type_dict = sorted(ast.literal_eval(p[6]).items(), key=lambda kv: kv[1], reverse=True)
        t = 0
        for k in sorted_camp_type_dict:
            t += k[1]
        
        for k in sorted_camp_type_dict:
            print(k[0], k[1]/t)

        print("----------")
    else:
        ax.scatter(p[0], p[1], p[2], c='r' , marker='o', s=1)

ax.scatter(0, 0, 0, c='b', marker='^')

ax.set_xlabel('Acc')
ax.set_ylabel('Ctr')
ax.set_zlabel('Retarget')

ax.view_init(elev= 10, azim=-75)

plt.show()
