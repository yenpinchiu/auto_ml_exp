import shelve
import random

from utils import *

if __name__ == "__main__":
    company = "cathaybk"

    train_campaign_db = shelve.open("{}_train_campaign_db".format(company))
    test_campaign_db = shelve.open("{}_test_campaign_db".format(company))
    current_campaign_db = shelve.open("{}_current_campaign_db".format(company))
    retarget_db = shelve.open("{}_retarget_db".format(company))
    feature_db = shelve.open("{}_feature_db".format(company))

    train_features = build_train_features(train_campaign_db, feature_db)

    #camp_type_list = build_camp_type_list([train_campaign_db, test_campaign_db, current_campaign_db])
    camp_type_list = {'ec:clothes', 'Real Estate: Buying/Selling Homes', 'EC: Female Clothes & Shoes', 'Utility', 'Game: Sports games', 'education:language', 'Insurance', 'EC: Home & Garden', 'dating', 'Cosmetics', 'Game: Kung-Fu', 'ec:food', 'Game: Music Game', 'Cell Phone/Pad', 'Game: Three Kingdoms', 'Luxury Car', 'Game', 'Game: Arcade and action games', 'Financing & Banking - Personal Investment', 'Utility: Delivery', 'Education: Abroad studying', 'ec:cosmetics', 'Travel', 'Skin Care', 'Airlines', 'Telecom', 'Movie', 'financing: personal', 'Game: Female Romantic Game', 'Automobile', 'Utility: News', 'Education', 'Real Estate', 'sell cars', 'Travel: Purchase ticket', 'EC: B2B2C Mall', 'financing:credit card', 'Mother & Baby (Diaper)', 'Style & Fashion:Clothing', 'Style & Fashion:Accessories', 'ec:electronics', 'Dating: Pairing (no porn)', 'Game: Male Romantic Game', 'e-commerce', 'Utility: Magazine', 'Style & Fashion:Jewelry', 'Travel: Business Travel', 'Job Fairs', 'Food & Drink', 'EC: Accessories', 'Travel: Hotels', 'Style & Fashion: Shoes', 'Travel: Order', 'Card Game', 'EC: Health', 'Utility: Coupon', 'Lottery & Gambling', 'Style & Fashion: Furniture', 'Shopping', 'Financing', 'Sensitive', 'financing:corporate', 'Style & Fashion:Beauty', 'Health', 'Game: Casual games', 'Game: RPG', 'Economy Car'}

    similarity_score_total = 0
    adid_count = 0
    for adid in test_campaign_db:
        if adid not in feature_db:
                continue

        similarity_score = caculate_similarity_score(adid, process_feature(feature_db[adid]), train_features)
        similarity_score_total += similarity_score
        adid_count += 1

        if adid_count % 50000 == 0:
            break

    similarity_n = 1 / (similarity_score_total/adid_count)
    print("score normalization: sim:{}".format(similarity_n))

    points_num = 1000
    camp_type_weights = {i: {c: random.random() for c in camp_type_list} for i in range(0, points_num)}

    metric_points = {i: [camp_type_weights[i], Statistics(), {camp: 0.0 for camp in camp_type_list}] for i in camp_type_weights}

    result_stats_dic = {}
    for campaign_db in [test_campaign_db, train_campaign_db]:
        for i in metric_points:
            metric_points[i][1] = Statistics()

        adid_count = 0
        for adid in campaign_db:
            if adid not in feature_db:
                continue

            similarity_score = caculate_similarity_score(adid, process_feature(feature_db[adid]), train_features)
            ctr_cvr_score, click_after_show_num, show_num = caculate_ctr_cvr_score(adid, campaign_db[adid][:4])
            retarget_score, is_retarget = caculate_retarget_score(adid, retarget_db)

            adid_camp_types = campaign_db[adid][4]
            adid_camp_types_num = len(adid_camp_types)
            for i in metric_points:
                mp = metric_points[i]

                adid_camp_type_weights = [mp[0][camp] for camp in adid_camp_types]
                if len(adid_camp_type_weights) != 0:
                    adid_avg_camp_type_weight = sum(adid_camp_type_weights) / len(adid_camp_type_weights)
                else:
                    adid_avg_camp_type_weight = 0.5 #如果都沒有有click的campaign,那就放個期望值0.5吧

                score_threshold = 0.75 #adid_avg_camp_type_weight的期望值大約是0.5,所以score也會在0.5上下,threshold取1可以把percentage壓在x%~1x%,還可以
                similarity_w = 1 * similarity_n * adid_avg_camp_type_weight

                similarity_score_reweight = similarity_w * similarity_score
                score = similarity_score_reweight
                
                mp[1].score_total += score
                mp[1].similarity_score_total += similarity_score_reweight

                if score >= score_threshold:
                    mp[1].output_score_total += score
                    mp[1].output_similarity_score_total += similarity_score_reweight
                    
                    mp[1].output_click_after_show_num_total += click_after_show_num
                    mp[1].output_show_num_total += show_num
                    mp[1].output_retarget_total +=  1 if is_retarget else 0

                    for camp_cate in adid_camp_types:
                        mp[2][camp_cate] += 1 / adid_camp_types_num
                    
                    mp[1].output_adid_count += 1

            adid_count += 1

            if adid_count % 10000 == 0:
                print(adid_count)

            if adid_count % 500000 == 0:
                break
        
        for i in metric_points:
            mp = metric_points[i]
            '''
            (print("output_percentage: {}, avg score: {}/{}, avg sim: {}/{}"
                .format(
                        mp[1].output_adid_count/adid_count, 
                        mp[1].score_total/adid_count, 
                        mp[1].output_score_total/mp[1].output_adid_count, 
                        mp[1].similarity_score_total/adid_count, 
                        mp[1].output_similarity_score_total/mp[1].output_adid_count
                    )
                )
            )

            (print("output_percentage: {}, output_ctr: {}, output_retarget: {}"
                .format(
                        mp[1].output_adid_count/adid_count, 
                        mp[1].output_click_after_show_num_total/mp[1].output_show_num_total, 
                        mp[1].output_retarget_total/mp[1].output_adid_count
                    )
                )
            )

            (print("{} {} {}".format(
                        mp[1].output_adid_count/adid_count, 
                        mp[1].output_click_after_show_num_total/mp[1].output_show_num_total, 
                        mp[1].output_retarget_total/mp[1].output_adid_count
                    )
                )
            )
            '''
            if i not in result_stats_dic:
                result_stats_dic.update({i: [0.0, 0.0, 0.0, {camp_cate: mp[2][camp_cate]for camp_cate in mp[2]}]})
                result_stats_dic[i][1] = mp[1].output_click_after_show_num_total/mp[1].output_show_num_total
                result_stats_dic[i][2] = mp[1].output_retarget_total/mp[1].output_adid_count
            else:
                result_stats_dic[i][0] = mp[1].output_adid_count/adid_count

    for i in result_stats_dic:
        print("{}|{}|{}|{}".format(result_stats_dic[i][0], result_stats_dic[i][1], result_stats_dic[i][2], result_stats_dic[i][3]))

    current_metric_point(train_campaign_db, test_campaign_db, current_campaign_db, retarget_db)

    train_campaign_db.close()
    test_campaign_db.close()
    current_campaign_db.close()
    retarget_db.close()
    feature_db.close()
