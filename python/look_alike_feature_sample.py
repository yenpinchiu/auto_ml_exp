#chinghua.yang給的範例code

#一筆一筆去data base裡查各id的lookalike feature,如果要取大量會比較慢
#39048087個id (look alike predict set的大小數量級)大約要52小時
'''
from aixon.sources.data_handlers.utils.dmp_data import DmpData
with open('/nfs/tmp/cathaybk_test') as f:
    seeds = [i.strip() for i in f]

dmp_feature_dir = '/nfs/lookalike/tw/2018-12-21'
dmp_data = DmpData(dmp_feature_dir)
max_n_positives = len(seeds)
max_n_negatives = 0
result_ids, data, column_names = dmp_data.get_persona_feature(seeds, max_n_positives, max_n_negatives)
'''

#一次讀全部test data的
'''
from typing import Mapping

import scipy.sparse as sp
import numpy as np
import h5sparse
from bistiming import SimpleTimer

from aixon.sources.data_handlers.utils.dmp_data import DmpData


def csr_transform_column_index(
        matrix: sp.csr_matrix, column_index_map: Mapping[int, int], n_columns: int):
    """Transform a CSR matrix to a new column order.

    Parameters
    ----------
    matrix: sp.csr_matrix
        The original matrix.
    column_index_map: Mapping[int, int]
        Map from old index to new index. If a new column is not mapped by any old column, it will
        be all zero.
    n_columns: int
        The number of columns in the new matrix. It should be greater or equal to the max value in
        ``column_index_map``.
    """
    # map column indices
    old_col_idx, new_col_idx = list(zip(*sorted(column_index_map.items())))
    use_data = np.isin(matrix.indices, old_col_idx)
    csr_indices = np.take(
        new_col_idx,
        np.searchsorted(old_col_idx, matrix.indices[use_data]))

    # filter data
    csr_data = matrix.data[use_data]

    # build indptr
    csr_indptr = np.empty_like(matrix.indptr)
    csr_indptr[0] = 0
    indptr = 0
    row_indptr_start = matrix.indptr[0]
    for row_i, row_indptr_end in enumerate(matrix.indptr[1:]):
        indptr += np.count_nonzero(use_data[row_indptr_start:row_indptr_end])
        csr_indptr[row_i + 1] = indptr
        row_indptr_start = row_indptr_end
    assert csr_indptr[-1] == csr_indices.shape[0]

    # build CSR matrix
    result_matrix = sp.csr_matrix(
        (csr_data, csr_indices, csr_indptr),
        shape=(matrix.shape[0], n_columns), dtype=np.float32)
    result_matrix.sort_indices()
    return result_matrix


def iter_testing_feature_in_h5(dmp_h5_path, batch_size=1000000):
    with h5sparse.File(dmp_h5_path, 'r') as h5f:
        # build the map from full index to used index
        column_names = np.char.decode(h5f['column_names'].value, 'utf-8')
        col_idx_map = {col: col_i for col_i, col in enumerate(column_names)}
        # map from the column idx in h5 to the column idx used in training
        used_idx_map = {col_idx_map[col]: col_i
                        for col_i, col in enumerate(column_names)
                        if col in col_idx_map}

        dmp_data_shape = h5f['data'].h5py_group.attrs['h5sparse_shape']
        for batch_i, batch_start in enumerate(range(0, dmp_data_shape[0], batch_size)):
            # get one batch of data
            batch_end = min(dmp_data_shape[0], batch_start + batch_size)
            with SimpleTimer('Reading dmp feature h5', verbose_start=False):
                batch_data = h5f['data'][batch_start: batch_end]

            # select the used features and build a new CSR sparse matrix
            with SimpleTimer('Building test feature', verbose_start=False):
                batch_feature = csr_transform_column_index(
                    batch_data, used_idx_map, len(column_names))

            yield batch_feature


dmp_data = DmpData('/nfs/lookalike/tw/2018-12-21/')
dmp_h5_path = '/nfs/lookalike/tw/2018-12-21/data.h5'
output_device_idx = None
offset = 0
for features in iter_testing_feature_in_h5(dmp_h5_path):
    offset += features.shape[0]
    device_idx = np.arange(offset, offset + features.shape[0], dtype=np.int64)
    if output_device_idx is None:
        output_device_idx = device_idx
    else:
        output_device_idx = np.concatenate((output_device_idx, device_idx))
    print(offset)
    # if offset > 0:
    #     break

sorted_idx = output_device_idx.argsort()
output_device_idx = output_device_idx[sorted_idx]

# get device IDs from device idx
device_ids = dmp_data.convert_device_dmp_h5_idx_to_ids(output_device_idx)
'''
