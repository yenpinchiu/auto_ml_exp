package com.appier.auto_ml_exp

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._

import org.apache.spark.sql.types.DoubleType

//由於各公司並沒有把我們產生的lookalike list拿去在我們這邊打,因此其實我們不知道該list真正用起來的ctr cvr,故是simulation
//simulation方法是算這些id在我們自己這邊的ctr cvr,希望能看出我們simulation的ctr cvr和feedback有正向關係
object LookAlikeOutputCTRCVRSimulation {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption
        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val ss = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import ss.implicits._

        val company_list = Seq("sega", "neogence", "cathaybk", "igs_tw", "igs_jp")

        for (company <- company_list){
            var metric_source_campaign_current = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_current".format(company)
            )

            //每間公司有自己的類型,估計拿去用的話應該就是用在該類型,因此理論上只用我們這邊該類型的camp算會更準,故作篩選
            if (company == "sega" || company == "igs_tw" || company == "igs_jp"){
                metric_source_campaign_current = metric_source_campaign_current
                    .filter($"category" === "Game" || $"category".contains("Game:") || $"category" === "Card Game")
            }
            else if (company == "neogence"){
                metric_source_campaign_current = metric_source_campaign_current
                    .filter($"category" === "Skin Care")
            }
            else if (company == "cathaybk"){
                metric_source_campaign_current = metric_source_campaign_current
                    .filter($"category" === "Financing" || $"category".contains("financing:") || $"category" === "Financing & Banking - Personal Investment")
            }
            else{
                throw new Exception("unrecognized company")
            }

            metric_source_campaign_current = metric_source_campaign_current
                .agg(
                    sum($"show") as "show", 
                    sum($"clickAfterShow") as "clickAfterShow", 
                    sum($"click") as "click", 
                    sum($"action") as "action"
                )
                .select(
                    $"clickAfterShow".cast(DoubleType) / $"show".cast(DoubleType) as "ctr", 
                    $"action".cast(DoubleType) / $"click".cast(DoubleType) as "cvr"
                )
            
            metric_source_campaign_current
                .show(10, false)
        }
    }
}
