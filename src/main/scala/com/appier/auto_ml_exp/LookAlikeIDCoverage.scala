package com.appier.auto_ml_exp

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._

import org.apache.spark.sql.types.DoubleType

//計算lookalike test list和output list在一段時間內真的出現在客戶網站的coverage
object LookAlikeIDCoverage {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption
        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val ss = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import ss.implicits._

        val company_list = Seq("sega", "neogence", "cathaybk", "igs_tw", "igs_jp")

        for (company <- company_list){
            val metric_source_campaign_test = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_test".format(company)
            )
        
            val test_id_count = metric_source_campaign_test
                .groupBy($"id")
                .count()
                .count()

            var metric_source_campaign_current = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_current".format(company)
            )

            val current_id_count = metric_source_campaign_current
                .groupBy($"id")
                .count()
                .count()

            val metric_source_retarget_test = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=retarget_test".format(company)
            )
        
            val ret_test_id_count = metric_source_retarget_test
                .groupBy($"id")
                .count()
                .count()

            val metric_source_retarget_current = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=retarget_current".format(company)
            )
        
            val ret_current_id_count = metric_source_retarget_current
                .groupBy($"id")
                .count()
                .count()

            println((ret_current_id_count.toDouble / (ret_test_id_count * current_id_count)) * 1000000)
            println((ret_current_id_count.toDouble / current_id_count) * 1000)
        }
    }
}
