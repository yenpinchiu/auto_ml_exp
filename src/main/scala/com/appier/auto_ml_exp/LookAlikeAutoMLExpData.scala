package com.appier.auto_ml_exp

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._

import org.apache.spark.sql.types.DoubleType

//產生look alike auto ml需要的data
object LookAlikeAutoMLExpData {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption
        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val ss = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import ss.implicits._

        val company_list = Seq("sega", "neogence", "cathaybk", "igs_tw", "igs_jp")

        for (company <- company_list){
            for (campaign_type <- Seq("train", "test", "current")){
                //不確定在這份資料中會不會有show=0,或四者都=0的資料,不過有也沒關係,和它是否要加入資料中無關
                //都是0的資料仍是一個等待test的current候選或會被加入train的adid,比方說新的adid就一定都是0
                //這樣只表示它ctr cvr score必定是0而已,只要有browse等資料就應該有aixon feature,就能算similarity score
                ss
                    .read.parquet("s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_%s".format(company, campaign_type))
                    .groupBy($"id", $"category")
                    .agg(
                        sum($"show") as "show", 
                        sum($"clickAfterShow") as "clickAfterShow", 
                        sum($"click") as "click", 
                        sum($"action") as "action"
                    )
                    .select(
                        $"id", 
                        array($"category", $"show", $"clickAfterShow", $"click", $"action") as "metric"
                    )
                    .groupBy($"id")
                    .agg(
                        collect_list("metric") as "metrics"
                    )
                    .select(
                        udfMetricDataStr($"id", $"metrics") as "adid_metric_str"
                    )
                    .write
                    .mode(SaveMode.Overwrite)
                    .text("s3a://appier-cd-automl/tmp/%s_campaign_%s".format(company, campaign_type))
            }

            //igs_jp retarget資料是缺的,因為test和current內的id根本就沒出現
            if (company != "igs_jp"){
                for (retarget_type <- Seq("test", "current")){
                    ss
                        .read.parquet("s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=retarget_%s".format(company, retarget_type))
                        .groupBy($"id")
                        .count()
                        .select(
                            $"id"
                        )
                        .write
                        .mode(SaveMode.Overwrite)
                        .text("s3a://appier-cd-automl/tmp/%s_retarget_%s".format(company, retarget_type))
                }
            }
        }
    }

    val udfMetricDataStr = udf(metricDataStr _)
    def metricDataStr(adid: String, metrics: Seq[Seq[String]]): String = {
        "%s||%s".format(
            adid, 
            metrics
                .map(x => "%s|%s|%s|%s|%s".format(x(0), x(1), x(2), x(3), x(4))) mkString "||"
        )
    }
}
