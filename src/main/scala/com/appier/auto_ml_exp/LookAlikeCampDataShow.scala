package com.appier.auto_ml_exp

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._

import org.apache.spark.sql.types.DoubleType

//這些是KY產生來用於幫助做look alike auto ml的資料
object LookAlikeCampDataShow {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption
        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val ss = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import ss.implicits._

        val company_list = Seq("sega", "neogence", "cathaybk", "igs_tw", "igs_jp")

        for (company <- company_list){
            //用來train lookalike的id (也就是廠商給的seed包),在我們自己campaign內的 imp clickaftershow click conversion數量
            //clickaftershow是點imp進來的click,click則是所有click,包含站內點來點去
            val metric_source_campaign_train = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_train".format(company)
            )

            metric_source_campaign_train
                .show(10, false)
            
            //全部用lookalike model做predict的ids,也就是test set,在我們自己campaign內的 imp clickaftershow click conversion數量
            //理論上就是所有我們有的id,所以每間公司的應該都要相同,但可能有些神秘條件篩選導致有差
            val metric_source_campaign_test = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_test".format(company)
            )
        
            metric_source_campaign_test
                .show(10, false)

            //lookalike model predict出來和seed包近似的ids,也就是predict結果,在我們自己campaign內的 imp clickaftershow click conversion數量
            //也就是predict結果中被認為和seed包較相近的id
            var metric_source_campaign_current = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=campaign_current".format(company)
            )

            metric_source_campaign_current
                .show(10, false)
           
            //test list中之後一段時間內有真正出現在客戶網站的id
            val metric_source_retarget_test = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=retarget_test".format(company)
            )
        
            metric_source_retarget_test
                .show(10, false)

            //output list中之後一段時間內有真正出現在客戶網站的id
            val metric_source_retarget_current = ss.read.parquet(
                "s3a://appier-cd-automl/out/metric_source/company_id=%s/dataset=retarget_current".format(company)
            )
        
            metric_source_retarget_current
                .show(10, false)
        }
    }
}
