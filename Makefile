SHELL := /bin/bash

# customize variable section
name=auto_ml_exp
release_folder=dist
spark_masters=master-cd-x
spark_master_ssh_key=id_rsa
conda_download_url=https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
# end of customize section

conda_root=/mnt/miniconda
venv_conda=$(conda_root)/bin/conda
venv_dir=$(conda_root)/envs/$(name)
venv_pip=$(venv_dir)/bin/pip
venv_python=$(venv_dir)/bin/python
venv_zip=$(release_folder)/env.tgz
version=$(shell ./bin/sbt -no-colors version|tail -1|awk '{print $$2}')

include_files_zip=$(release_folder)/files.tgz
upload_path=s3://appier-cd-release/release/$(name)
upload_path_stage=s3://appier-cd-release/stage/$(name)

temp_dir=$(release_folder)/temp

# compile the jars
build:
	./bin/sbt assembly

ci:
	./bin/sbt ~assembly

# publish the jars
pub:
	./bin/sbt publish

# run unit tests
test:
	./bin/sbt test

# remove artifacts during build
clean:
	@sudo rm -r $(release_folder)/*
	./bin/sbt clean

# set up virtualenv in local /mnt/miniconda
# this assumes miniconda have been installed already
env:
	@if [ ! -d $(conda_root) ] ; then \
		echo -e "\e[1m$(conda_root) is not exists, download conda and install\e[m"; \
		sudo wget $(conda_download_url) -O miniconda.sh; \
		echo -e "\e[1mRun install script...\e[m"; \
		sudo bash miniconda.sh -b -p $(conda_root); \
		echo -e "\e[1mRemove download file\e[m"; \
		sudo rm miniconda.sh; \
	else \
		echo -e "\e[1mMiniconda already installed at $(conda_root), skip.\e[m"; \
	fi
	@if [ ! -d $(venv_dir) ] ; then \
		@echo -e "\e[1mCreate Environment\e[m"; \
		sudo -H $(venv_conda) create --yes -p $(venv_dir) python=3.7; \
	fi
	@echo -e "\e[1mPip install...\e[m"
	sudo -H $(venv_pip) install --upgrade pip
	@if [ -e $(HOME)/.ssh/id_rsa ] ; then \
		sudo -H ssh-agent sh -c 'ssh-add $(HOME)/.ssh/id_rsa; GIT_SSH_COMMAND="ssh -i $(HOME)/.ssh/id_rsa" $(venv_pip) install -r requirements.txt'; \
	else \
		sudo -H $(venv_pip) install requirements.txt; \
	fi
	@echo -e "\e[1mLocal environment setup complete.\e[m"

# env remote setup
$(spark_masters):
	@echo -e "\e[1mSync env to $@...\e[m"
	ssh -o StrictHostKeyChecking=no -i ${HOME}/.ssh/${spark_master_ssh_key} yenpinchiu@$@.spark.appier.info  "sudo -i mkdir -p $(venv_dir)"
	sudo rsync -rlptDq --delete -e "ssh -i ${HOME}/.ssh/${spark_master_ssh_key}" --rsync-path='sudo -i rsync' $(venv_dir)/ yenpinchiu@$@.spark.appier.info:$(venv_dir)

# copy existed env to spark master
env-cp: $(spark_masters)

# delete local env
env-rm:
	sudo $(venv_conda) env remove -p $(venv_dir)

get-pip:
	@echo -e "\e[1m$(venv_pip)\e[m"

# setup python env
setup: env env-cp

# create python link to do env sync and run python
.python_link:
	@echo -e "\e[1mCreate python link\e[m"
	@if [ -f $(release_folder)/python ] ; then \
		sudo rm $(release_folder)/python; \
	fi
	@sudo sh -c 'printf "#!/bin/sh\n" > $(release_folder)/python'
	@sudo sh -c 'printf "if [ ! -d $(conda_root) ] ; then\n\tmkdir -p $(conda_root)\nfi\n" >> $(release_folder)/python'

	@sudo sh -c 'printf "rsync -aqr " >> $(release_folder)/python'

	@sudo sh -c 'printf $(conda_root) | sed "s/^.//" >> $(release_folder)/python'
	@sudo sh -c 'printf "/ " >> $(release_folder)/python'
	@sudo sh -c 'printf "$(conda_root)\n" >> $(release_folder)/python'
	@sudo sh -c 'readlink -f $(venv_python) | tr -d "\n" >> $(release_folder)/python'
	@sudo sh -c 'printf " \$$@ <&0\n" >> $(release_folder)/python'
	@sudo chmod 755 $(release_folder)/python

# create python link to do env sync and run python
.python_link_local:
	@echo -e "\e[1mCreate python local link\e[m"
	@if [ -f $(release_folder)/python_local ] ; then \
		sudo rm $(release_folder)/python_local; \
	fi
	@sudo sh -c 'printf "#!/bin/sh\n." > $(release_folder)/python_local'
	@sudo sh -c 'readlink -f $(venv_python) | tr -d "\n" >> $(release_folder)/python_local'
	@sudo sh -c 'printf " \$$@ <&0\n" >> $(release_folder)/python_local'
	@sudo chmod 755 $(release_folder)/python_local


.rm-temp:
	@if [ -d $(temp_dir) ] ; then \
		sudo rm -r $(temp_dir); \
	fi

.create-temp: .rm-temp
	@sudo mkdir -p $(temp_dir)

.tar-files: .create-temp
	@echo -e "\e[1mCopy files from include-list.txt\e[m"
	@while read -r line || [[ -n "$$line" ]] ; do \
		if [ "$${line:0:1}" != "#" ] && [ "$${line:0:1}" != "" ] ; then \
			echo -e "\e[0;36m Copy: $$line...\e[m"; \
			sudo cp -r $$line $(temp_dir); \
		fi ; \
		done < 'include-list.txt'
	$(eval zFile=$(shell basename $(include_files_zip) | tr -d '\n'))
	$(eval current_dir=$(shell pwd))
	@cd $(temp_dir); \
	sudo touch $(zFile); \
	sudo tar -czpf $(zFile) --owner=0 --group=0 . --exclude=$(zFile); \
	cd $(current_dir); \
	sudo mv $(temp_dir)/$(zFile) $(include_files_zip)
	@sudo $(MAKE) -s .rm-temp

.tar-env: .create-temp .python_link .python_link_local
	@echo -e "\e[1mCreate virtual env tar file\e[m"
	sudo tar -czpPf $(venv_zip) --owner=0 --group=0 $(venv_dir) -C $(release_folder) python python_local
	sudo rm $(release_folder)/python $(release_folder)/python_local
	@sudo $(MAKE) -s .rm-temp

# package miniconda env
package: .tar-env

# sync to s3
install: .tar-files
	@echo -e "\e[1mInstall to $(upload_path)\e[m"
	aws s3 sync --delete $(release_folder) $(upload_path)

# sync to s3
stage: .tar-files
	@echo -e "\e[1mInstall to $(upload_path_stage)\e[m"
	aws s3 sync --delete $(release_folder) $(upload_path_stage)
