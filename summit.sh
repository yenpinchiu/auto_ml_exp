#!/bin/bash

#eval "$(ssh-agent -s)"
#sudo ssh-add ~/.ssh/id_rsa

make setup
make package
make build
make install

virtualenv manually_summit_env
source manually_summit_env/bin/activate
pip install -r requirements.txt

SCRIPT=${1}
python summit.py --script $SCRIPT

deactivate
rm -rf manually_summit_env

#pkill ssh-agent
