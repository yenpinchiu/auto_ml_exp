#!/mnt/miniconda/envs/auto_ml_exp/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

from cron_utils.scheduler import submit_job as remote_submit
from cron_utils.scheduler import default_parser as remote_parser
from cron_utils.scheduler import spark_classpath as remote_classpath


def spark_classpath(stage=False):
    return remote_classpath(
        project='auto_ml_exp',
        stage=stage,
        package='auto_ml_exp',
        scala_version='2.11',
        version='0.1-spark-2.2.0-SNAPSHOT',
        suppress_search=True)


def local_class(cls):
    return 'com.appier.auto_ml_exp.' + cls


def submit_job(class_name,
               job_name,
               title,
               status,
               priority,
               retry,
               cmd_args='',
               args=None,
               inputs=None,
               outputs=None,
               host=None,
               pool=None,
               group=None,
               stage=False):
    classpath = spark_classpath(stage)
    s3_path = 's3://appier-cd-release/{env}/auto_ml_exp'.format(env='stage' if stage else 'release')

    remote_submit(
        classpath,
        class_name,
        job_name,
        title,
        status,
        priority,
        retry,
        s3_path,
        cmd_args,
        args,
        inputs,
        outputs,
        host,
        pool,
        group,
        timeout=8 * 60 * 60)


def default_parser(host='master-ai-general.spark.appier.info', group='auto_ml_exp'):
    return remote_parser(
        host=host, status='pending', priority=51, retry=0, pool=256, group=group)
